"""
Modify the TODO's to create a plugin.
Your plugin can do anything it wants with the messag it receives.
You are also free to add any imports you need to make your plugin work.
"""

class Plugin():
    async def on_send(self, message):
        # TODO modify the message variable and return what you
        # want to be sent. The message you receive will be of the form
        # {"action": "send", "message": {actual message}}
        # where something contains the actual message. Most likely the
        # actual message looks something like
        # {"type": "text", contents: "some text the user typed"}
        # but it may have been modified by other plugins before reaching
        # you. Because of this, you should avoid making too many assumptions
        # about the structure of the message.
        return message

    async def on_receive(self, message):
        # TODO modify the message variable and return what you
        # want to be sent. Message could be any dictionary so
        # you should verify that any keys you depend on actual
        # exist before using them. A client will display any message
        # which matches the form {"type": "text", "contents": "some text"}.
        return message

