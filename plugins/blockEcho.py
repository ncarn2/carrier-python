"""
This plugin filters out messages from the echo bot
"""

class Plugin():
    async def on_send(self, message):
        # This plugin doesn't modify any sent messages
        return message

    async def on_receive(self, message):
        # This plugin uses the "from" field to figure out if the
        # message was sent by echo. If so, the message won't be rendered
        # because the type isn't text.
        if message["from"] == "echo":
            return {"type": "empty"}
        return message

