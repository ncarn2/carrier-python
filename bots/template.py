"""
This is the client code for the python messenger.
The client is written using QT and websockets.
"""
import asyncio
import json
import websockets

class Main():
    """
    The main window for our client
    """
    def __init__(self, name):
        self.name = name
        self.websocket = None

    async def connect(self, location):
        """
        Called to start and run the bot
        """
        uri = f"ws://{location}:8765"
        websocket = await websockets.connect(uri)
        await websocket.recv()
        await websocket.send(self.name)
        response = await websocket.recv()
        if response == "?":
            print("Someone with the same name is already connected")
            exit()
        self.websocket = websocket
        await self.socket_loop()


    async def socket_loop(self):
        """
        The main loop for the websocket part of the program.
        Called by connect.
        """
        while True:
            raw_message = await self.websocket.recv()
            message = json.loads(raw_message)
            # Make sure we didn't send the message
            if message["from"] != self.name:
                await self.on_message(message)

    async def on_message(self, message):
        """
        TODO Begin here if writing a bot.
        Whenever the server sends a message, this function is called
        Messages that you send will not trigger this function so don't
        worry about getting stuck in a loop. You can do anything you
        want in here. If you want to send a message, use
        "await self.websocket.send(json.dumps(message))" where message
        is what you want to send.

        Message should be in the form of a dictionary. At the top level,
        It should always contain a key "action". Currently, the server
        only implements the "send" action. If you have the send action,
        you should also have a key "message" holding another dictionary.

        That dictionary should always contain a key "type" holding some string.
        If you want normal clients to see the message, "type" should point to
        "text" and another key, "content", should point to the contents of the
        message.

        For example, to send the word hello, message should be
        "{"action": "send", "message": {"type": "text", "content": "hello"}}"
        """

# Run the program
if __name__ == "__main__":
    # TODO Replace the empty string with the name of the bot
    main = Main("")
    # TODO Replace the URL with something other than localhost
    # once finished testing
    asyncio.get_event_loop().run_until_complete(main.connect("localhost"))
