"""
This is the client code for the python messenger.
The client is written using QT and websockets.
Knowing how the client code works shouldn't be needed
for defining plugins or bots.
"""
import os
import asyncio
import importlib
import json
import websockets

from PyQt5.QtWidgets import (
    QApplication,
    QWidget,
    QPushButton,
    QVBoxLayout,
    QLabel,
    QLineEdit,
    QMainWindow,
    QHBoxLayout,
    QInputDialog,
    QScrollArea,
    QListWidget,
    QListWidgetItem,
)
from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtCore import *
# We need anyncqt so that the older pyqt library can work with the
# newer async code
from asyncqt import QEventLoop, asyncSlot, asyncClose


async def new_main():
    """
    Initializes all of the gui elements and starts the
    connection with the server.
    """

    # Run all python code in the plugins folder and store it in plugins
    plugins = []
    for file in os.listdir("plugins"):
        # Get everything before the dot (aka everything but .py)
        # If the file lacks an extension, Python throws an error.
        # We don't really care and can just ignore the file in that case.
        try:
            name, file_type = file.split(".")
        except ValueError:
            pass
        # Is it a python file?
        if file_type == "py":
            # Import the file manually (as apposed to the normal imports up above)
            module = importlib.import_module(f"plugins.{name}")
            # Find the Plugin class in the file and create an instance of it
            plugins.append(getattr(module, "Plugin")())

    # Main is the QWidget which represents our entire window
    main = Main(plugins)
    main.show()


    # Prompt the user for the URL to connect to
    # Location is the location they want to connect to
    # good is true if the user actually submitted and false if they cancelled
    location, good = QInputDialog.getText(
        main, "Url", "Where should you connect to?", QLineEdit.Normal
    )

    # If the user doesn't give us something to connect to,
    # there's no point in continuing.
    if not good:
        exit()

    # Now that we have a URL, let's connect
    uri = f"ws://{location}:8765"
    # This will crash if the server can't be found
    websocket = await websockets.connect(uri)
    main.websocket = websocket

    # Once you connect to the server, the server sends a question mark
    # That's the client's cue to respond with a name
    response = await websocket.recv()
    # The server will continue to respond with ? until we give a valid name
    while response == "?":
        # Similar to location but for getting the user's name
        text, good = QInputDialog.getText(
            main, "name", "What's your name?", QLineEdit.Normal
        )

        if not good:
            exit()
        # Once we send a name, the server will respond with ? or ok
        # ? Means the name was already taken and we should try again
        # ok means the name is now ours and we can continue
        await websocket.send(text)
        response = await websocket.recv()
    # Right after connecting, the server sends us the last 100 messages
    # Let's get those and display them to the user
    await main.get_inital_messages()

    # We've finally finished setting things up. Now we can just wait
    # on either QT or the server to send us something to do.
    await main.socket_loop()


class Main(QMainWindow):
    """
    The main window for our client
    """
    def __init__(self, plugins):
        # Mandatory python code
        super().__init__()
        self.plugins = plugins

        # Initialize some variables
        # Webs holds all of the web UI widgets in our window
        self.webs = []
        # The websocket will be our connection to the server
        self.websocket = None

        # Let's create a pretty standard messenger UI.
        # The QT UI is structured like a tree with one element
        # at the root and 0 or more elements coming off of it.
        # At the root of our tree is a container whose children
        # will be layed out vertically
        self.layout = QVBoxLayout()

        # Inside of the root is another vertical box holding all
        # of the messages we receive. We want to make that vertical
        # box scrollable. We need to create a scroll area, create a
        # widget, and create a box layout. The box layout goes in
        # the widget and the widget goas in the scroll area.
        self.scroll_widget = QScrollArea()
        self.scroll_widget.setWidgetResizable(True)
        self.derp = QWidget()
        self.message_list = QVBoxLayout(self.derp)
        self.message_list.setSizeConstraint(QVBoxLayout.SetMinAndMaxSize)
        self.message_list.insertStretch(-1, 1)
        self.scroll_widget.setWidget(self.derp)
        self.layout.addWidget(self.scroll_widget, 200)

        # Below the messages is a horizontal box which will contain
        # the text editor and the send button.
        self.edit_layout = QHBoxLayout()
        self.layout.addLayout(self.edit_layout)

        # LineEdit = text editor
        self.message = QLineEdit()
        self.edit_layout.addWidget(self.message)
        # Send button
        self.button = QPushButton("send", self)
        # When you click the button, the on_btn function gets called
        self.button.clicked.connect(self.on_btn)
        self.edit_layout.addWidget(self.button)
        self.message.returnPressed.connect(self.button.click)

        # self.layout.setSizeConstraint(QVBoxLayout.SetFixedSize)

        # This widget represents the entire window and holds our
        # root layout. I guess that makes window the root, not
        # layout, but the distinction isn't super important now.
        window = QWidget()
        window.setLayout(self.layout)
        self.setCentralWidget(window)

    async def get_inital_messages(self):
        """
        Get the last 100 messages sent to the server before we connected
        """
        initial = await self.websocket.recv()

        # A message comes in as a json dictionary
        # First, we apply any plugins and filter it
        messages = []
        for message in json.loads(initial):
            for plugin in self.plugins:
                message = await plugin.on_receive(message)
            if message["type"] and message["type"] == "text":
                messages.append(message)

        # Then we take the filtered list and add it to the UI
        for i, message in enumerate(messages):
            # We only display messages of type "text"
            if message["type"] and message["type"] == "text":
                await self.add_web(i, message)


    async def socket_loop(self):
        """
        The main loop for the websocket part of the program
        """
        while True:
            raw_message = await self.websocket.recv()
            message = json.loads(raw_message)
            # We do the same check as above
            for plugin in self.plugins:
                message = await plugin.on_receive(message)

            if message["type"] and message["type"] == "text":
                await self.add_web(len(self.webs), message)

    def ready(self, v):
        """
        Sets the webview height equal to the height of the content
        """
        # By default, webviews try to take up as much space as
        # possible. If the user justs sends a line of text, we
        # don't want to waste a ton of space. This function is
        # called where v is the result of some javascript. The
        # javascript returns the index into webs, a space, and
        # the height of the content inside the webview.
        i, h = v.split()
        if v:
            self.webs[int(i)].setFixedHeight(min(int(h) + 25, 175))
        # self.scroll_widget.verticalScrollBar().setValue(self.scroll_widget.verticalScrollBar().maximum())
        self.scroll_widget.ensureVisible(10000, 10000)

    @asyncSlot()
    async def on_btn(self):
        """
        Called when the user clicks the send button
        """
        # Hide the button until we finish sending so they can't
        # send it twice on accident.
        self.button.setEnabled(False)

        # Get the text they entered in the field then reset it
        text = self.message.text()
        self.message.setText("")

        # Create the dictionary we'll send to the server
        message = {"action": "send", "message": {"type": "text", "content": text}}
        for plugin in self.plugins:
            message = await plugin.on_send(message)

        # Send it then reset the button back to default
        await self.websocket.send(json.dumps(message))
        self.button.setEnabled(True)

    async def add_web(self, i, message):
        """
        Add a new message as a webview
        """
        # QT calls their webviews QWebEngineView.
        # I think this uses Chromium behind the scenes.
        row = QVBoxLayout()
        user = QLabel()
        user.setText(message["from"] + ":")
        row.addWidget(user)
        web = QWebEngineView()
        row.addWidget(web)
        self.webs.append(web)

        # Wrap the message in the root element
        web.setHtml(
            f"<div id=\"root\">{message['content']}</div>"
        )

        # See the explanation of ready from up above.
        # Once the page is loaded and ready to be displayed,
        # run some javascript to find the root element and determine
        # its height.
        web.loadFinished.connect(
            lambda ok: web.page().runJavaScript(
                str(i) + "+ \" \" + document.getElementById('root').scrollHeight;",
                self.ready,
            )
        )

        self.message_list.addLayout(row)


# Run the program
if __name__ == "__main__":
    # Start our QT app
    app = QApplication([])

    # Create an event loop which is compatible with async/await
    loop = QEventLoop(app)

    # Start the loops
    asyncio.set_event_loop(loop)
    asyncio.get_event_loop().run_until_complete(new_main())
